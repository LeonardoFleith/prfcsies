<?php

include_once "../classes/Solo.php";
include_once 'valida.php';

$novo_tipo_solo = valida($_POST['novo_tipo_solo']);
$novo_cap_campo = valida($_POST['novo_cap_campo']);
$id_solo = $_POST['id_solo'];


if (!empty($novo_tipo_solo) && !empty($novo_cap_campo)) {

	$update =  new Solo();
    $resultado = $update -> updateSolo($novo_cap_campo, $novo_tipo_solo, $id_solo);

	if ($resultado == true) {

	$mensagens[1] = "Seu solo foi alterada, aguarde liberação";
				header("location:../interface/templates/dashboard.php?pgs=alterar_solo.php&id_solo=$id_solo&mensagem=".$mensagens[1]);


	}else{
		
		$erros[1]="Solo não pode ser alterada";
		header("location:../interface/templates/dashboard.php?pgs=alterar_solo.php&id_solo=$id_solo&erro=".$erros[1]);
		
	}
}else{
	$erros[1]="Campo não preenchido";
	header("location:../interface/templates/dashboard.php?pgs=alterar_solo.php&id_solo=$id_solo&$novo_tipo_solo&$nova_cap_campo&$id_solo&erro=".$erros[1]);
}

