<?php

include_once "../classes/Cultura.php";
include_once "status.php";
include_once 'valida.php';


$tipo_planta =valida($_POST['tipo_planta']);
$nome_cientifico = valida($_POST['nome_cientifico']);
$tipo_user =  valida($_POST['tipo_user']);
$id_user =  valida($_POST['id_user']);
$status = status($tipo_user);


if (!empty($tipo_planta) || !empty($nome_cientifico)) {
	session_start();
    $_SESSION['campos_cultura'] = $_POST;

	if (!empty($tipo_planta)) {
		
		if (!empty($nome_cientifico)) {
			
			if ($tipo_user == 2) {

				$cadastrar = new Cultura();
				$cadastrar -> cadastracultura($tipo_planta, $status, $id_user, $nome_cientifico );	
				unset( $_SESSION['campos_cultura'] );

				$mensagens[1] = "Sua cultura foi solicitada com sucesso";
				header("location:../interface/templates/dashboard.php?pgs=cadastro_cultura.php&mensagem=".$mensagens[1]);

			}else{
			$erros[1] = "Houve algum problema na solicitação, tente novamente! ";
			header("location:../interface/templates/dashboard.php?pgs=cadastro_cultura.php&erro=".$erros[1]);
			}
		}else{
			$erros[1] = "Campo do nome científico está vazio";
			header("location:../interface/templates/dashboard.php?pgs=cadastro_cultura.php&erro=".$erros[1]);
		}
	}else{
		$erros[1] = "Campo tipo planta está vazio";
		header("location:../interface/templates/dashboard.php?pgs=cadastro_cultura.php&erro=".$erros[1]);
	}
}else{
	$erros[1] = "Campos tipo planta e nome científico estão vazios";
	header("location:../interface/templates/dashboard.php?pgs=cadastro_cultura.php&erro=".$erros[1]);
}
