	<?php

	include_once "../classes/Solo.php";
	include_once "status.php";
	include_once 'valida.php';

	$tipo_solo = valida($_POST['tipo_solo']);
	$cap_campo = valida($_POST['cap_campo']);
	$tipo_user = valida($_POST['tipo_user']);
	$id_user = valida($_POST['id_user']);
	$status = status($tipo_user);

if (!empty($tipo_solo) || !empty($cap_campo)) {

	session_start();
    $_SESSION['campos_solo'] = $_POST;

	if (!empty($tipo_solo)) {

		if (!empty($cap_campo)) {

			if ($cap_campo > 0  && filter_var($cap_campo, FILTER_VALIDATE_INT)) {

				if ($tipo_user == 2 ) {

					$cadastrar = new Solo();

					$cadastrar -> cadastrasolo($tipo_solo,$cap_campo,$id_user,$status);

					unset( $_SESSION['campos_solo'] );

					$mensagens[1] = "Seu solo foi solicitado com sucesso";
					header("location:../interface/templates/dashboard.php?pgs=cadastro_solo.php&mensagem=".$mensagens[1]);


				} else{
					echo("<script type='text/javascript'> alert( 'Você é um administrador, não pode cadastrar:)' );
						location.href='../interface/templates/dashboard.php?pgs=cadastro_solo_Admin.php';</script>");

				}

			}else {
				$erros[1] = "A capacidade de campo deve ser um número positivo";
				header("location:../interface/templates/dashboard.php?pgs=cadastro_solo.php&erro=".$erros[1]);
			}

		}else{
			$erros[1] = "Campo capacidade de campo está vazio";
			header("location:../interface/templates/dashboard.php?pgs=cadastro_solo.php&erro=".$erros[1]);
		}

	}else{
		$erros[1] = "Campo tipo solo está vazio";
		header("location:../interface/templates/dashboard.php?pgs=cadastro_solo.php&erro=".$erros[1]);
	}

}else{
	$erros[1] = "Campos tipo solo e capacidade campo estão vazios";
	header("location:../interface/templates/dashboard.php?pgs=cadastro_solo.php&erro=".$erros[1]);
}

?>