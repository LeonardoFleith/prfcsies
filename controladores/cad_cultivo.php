<?php

include_once "../classes/Cultivo.php";
include_once"../classes/Solo.php";
include_once"../classes/Cultura.php";
include_once 'valida.php';


$id_solo = valida($_POST['id_solo']);
$id_cultura = valida($_POST['id_cultura']);
$id_user = valida($_POST['id_user']);
$nome_cultivo = valida($_POST['nome_cultivo']);

$solo = new Solo();
$cod_solo = $solo -> consultaID_solo($id_solo);
$cod_solo = $cod_solo['cod_usuario'] ;

$cultura = new Cultura();
$cod_cultura = $cultura -> consultaID_cultura($id_cultura);
$cod_cultura = $cod_cultura['cod_usuario'];


if(!empty($id_cultura) || !empty($id_solo) || !empty($nome_cultivo)){
	session_start();
    $_SESSION['campos_cultivo'] = $_POST;

	if (!empty($id_cultura) && ($id_cultura!= 'Selecione...')) {
		
		if (!empty($id_solo) && ($id_solo!= 'Selecione...')) {
			
			if (!empty($nome_cultivo)) {

				if ( $cod_solo == $cod_cultura ) {

					$cadastrar = new Cultivo();

					$cadastrar -> cadastraCultivo($nome_cultivo,  $id_cultura, $id_solo);

					$mensagens[1] = "Seu cultivo foi cadastrado";
					header("location:../interface/templates/dashboard.php?pgs=cadastro_cultivo.php&mensagem=".$mensagens[1]);

					unset( $_SESSION['campos_cultivo'] );
				}else{
					$erros[1] = "Este solo ou cultura não é seu :(' ";
					header("location:../interface/templates/dashboard.php?pgs=cadastro_cultivo.php&erro=".$erros[1]);
				}
			}else{
				$erros[1] = "Campo nome cultivo está vazio ";
				header("location:../interface/templates/dashboard.php?pgs=cadastro_cultivo.php&erro=".$erros[1]);
			}
		}else{
			$erros[1] = "Campo nome solo está vazio ";
			header("location:../interface/templates/dashboard.php?pgs=cadastro_cultivo.php&erro=".$erros[1]);
			}
	}else{
		$erros[1] = "Campo nome cultura está vazio ";
		header("location:../interface/templates/dashboard.php?pgs=cadastro_cultivo.php&erro=".$erros[1]);
		}	
}else{
	$erros[1] = "Campos nome solo, nome cultura e nome cultivo estão vazios ";
	header("location:../interface/templates/dashboard.php?pgs=cadastro_cultivo.php&erro=".$erros[1]);
}	
