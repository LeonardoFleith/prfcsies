<?php

include_once "../classes/Cultivo.php";
include_once 'valida.php';


$novo_nome_cultivo = valida($_POST['novo_nome_cultivo']);
$novo_id_solo = valida($_POST['novo_id_solo']);
$novo_id_cultura = valida($_POST['novo_id_cultura']);
$id_cultivo = $_POST['id_cultivo'];


if (!empty($novo_id_cultura) && !empty($novo_id_solo) && !empty($novo_nome_cultivo)) {

	$update =  new Cultivo();
	$resultado = $update -> updateCultivo($novo_nome_cultivo, $id_cultivo, $novo_id_solo, 
	$novo_id_cultura);


	if ($resultado == true) {

	echo ("<script type='text/javascript'> alert('Cultivo alterado com sucesso'); location.href='../interface/templates/dashboard.php?pgs=cadastrados_cultivo.php&pg=1'; </script>");

	}else{
		$erros[1]="Cultivo não pode ser alterado";
		header("location:../interface/templates/dashboard.php?pgs=alterar_cultivo.php&id_cultivo=$id_cultivo&erro=".$erros[1]);
		
	}
}else{
	$erros[1]="Campo não preenchido";
	header("location:../interface/templates/dashboard.php?pgs=alterar_cultivo.php&id_cultivo=$id_cultivo&erro=".$erros[1]);
		
}