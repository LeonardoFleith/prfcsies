
<?php  
include_once '../classes/Usuario.php';
include_once 'valida.php';
include_once '../classes/Login.php';
include_once '../classes/Databases.php';

$nome = valida($_POST['nome']);
$usuario = valida($_POST['usuario']);
$email = filter_input(INPUT_POST,'email');
$senha = valida($_POST['senha']);
$confirmar_senha = valida($_POST['confirmar_senha']);
$Error           = true;

$conexao     = Databases::getConnection();
$consulta    = "SELECT email,usuario FROM usuario WHERE email = '$email'";
$consultando = $conexao->query($consulta); 
$resultado   = $consultando->fetch(PDO::FETCH_OBJ);


if (!empty($nome) || !empty($usuario) || !empty($email) || !empty($senha) || !empty($confirmar_senha)) {

    @session_start();
    $_SESSION['campos_cadastro'] = $_POST;

    if (!empty($nome)) {

        if (!empty($usuario)) {

            if (!empty($email)) {

                if ($resultado == false) {

                  if (!empty($senha)) {

                   if (strlen($senha) >= 8) {

                    if (preg_match( '/[0-9]/' , $senha) ) {

                        if (preg_match( '/[a-zA-Z]/' , $senha) ) {

                            if (!empty($confirmar_senha)) {

                             if (filter_var($email, FILTER_VALIDATE_EMAIL)) {

                                 if ($senha == $confirmar_senha){

                                    $conexao = Databases::getConnection();

                                    /*Verifica se já há um usuário cadastrado com este login*/
                                    $verificacao = "SELECT usuario FROM usuario where usuario='$usuario' and email='$email'";
                                    $retorna = $conexao->query($verificacao);
                                    $verificacao = $retorna->fetch(PDO::FETCH_ASSOC);

                                    if($verificacao == false){
                                     $erros[0] = "Seu usuário ou email já está cadastrado";
                                        }
                                    if ($verificacao['email'] != $email and $verificacao['usuario'] != $usuario) {

                                        //Enviando o e-mail
                                         $Para         = $email;
                                        $Titulo_email = 'Confirmação de cadastro';
                                        $Mensagem     = 'Olá '.$nome.', sua conta foi cadastrada com sucesso.';
                                        $headers      = 'From: '.'sies@sies.com'. "\r\n";
                                        $Bool         = mail($Para, $Titulo_email, $Mensagem, $headers);

                                        /*Cadastra as informações do novo usuário no banco*/

                                        $cadastrar =  new Usuario($usuario,$senha,$nome,$email);
                                        $cadastrar -> cadastrausuario($usuario,$senha,$nome,$email);
                                        unset( $_SESSION['campos_cadastro'] );

                                            if($cadastrar == true){
                                                /*Faz o login*/
                                                $logar = new Login();
                                                $logar->efetuaLogin($usuario, $senha);
                                            }
                                            //Verificação
                                            if($Bool){
                                                $Error = false;
                                            }

                                        echo("<script type='text/javascript'> alert( 'Parabéns, seu usuário foi cadastrado com sucesso! :)' );
                                        location.href='../interface/templates/dashboard.php?pgs=cadastrados_solo.php&pg=1'</script>");

                                        }else{

                                            $erros[0]=  "Já existe alguém cadastrado com este usuario ou email! Tente novamente :)" ;
                                            header("location:../interface/templates/cadastro_usuario.php?&erro=".$erros[0]);
                                            }
                                    }else{

                                        $erros[0] = "Senha está diferente da senha de confirmação";
                                        header("location:../interface/templates/cadastro_usuario.php?&erro=".$erros[0]);
                                        }
                                }else{
                                    $erros[0] = "Seu email está errado";
                                    echo("<script type='text/javascript'> alert( 'Seu formato de email está errado!Tente novamente :)' );
                                    location.href='../interface/templates/cadastro_usuario.php?&erro=".$erros[0]."';</script>");
                                    }
                            }else{
                                $erros[0] = "Campo confirmar senha está vazio";
                                header("location:../interface/templates/cadastro_usuario.php?&erro=".$erros[0]);
                                }

                        }else{
                            $erros[0] = "A senha deve ser composta com letras";
                            header("location:../interface/templates/cadastro_usuario.php?&erro=".$erros[0]);
                        }

                    }else{
                        $erros[0] = "A senha deve ser composta por numeros e letras";
                        header("location:../interface/templates/cadastro_usuario.php?&erro=".$erros[0]);
                        }
                   }else{
                    $erros[0] = "A senha deve ser composta por no minimo 8 caracteres";
                    header("location:../interface/templates/cadastro_usuario.php?&erro=".$erros[0]);
                   }    
                  }else{
                    $erros[0] = "Campo senha está vazio";
                    header("location:../interface/templates/cadastro_usuario.php?&erro=".$erros[0]);
                  }
                }else{
                     $erros[0] = "Este email já foi cadastrado";
                    header("location:../interface/templates/cadastro_usuario.php?&erro=".$erros[0]);
                }
            }else{
                $erros[0] = "Há campos vazios";
                header("location:../interface/templates/cadastro_usuario.php?&erro=".$erros[0]);
            }
        }else{
            $erros[0] = "Há campos vazios";
            header("location:../interface/templates/cadastro_usuario.php?&erro=".$erros[0]);
        }
    }else{
        $erros[0] = "Há campos vazios";
        header("location:../interface/templates/cadastro_usuario.php?&erro=".$erros[0]);
        }
}else{
    $erros[0] = "Campos estão vazios";
    header("location:../interface/templates/cadastro_usuario.php?&erro=".$erros[0]);
}