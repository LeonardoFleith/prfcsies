<?php

include_once "../classes/Cultura.php";
include_once 'valida.php';


$novo_nome_cientifico = valida($_POST['novo_nome_cientifico']);
$novo_tipo_planta = valida($_POST['novo_tipo_planta']);
$id_cultura = $_POST['id_cultura'];


if (!empty($novo_tipo_planta) && !empty($novo_nome_cientifico)) {

	$update =  new Cultura();
    $resultado = $update -> updateCultura($novo_nome_cientifico, $novo_tipo_planta, $id_cultura);

	if ($resultado == true) {
	$mensagens[1] = "Sua cultura foi alterada, aguarde liberação";
				header("location:../interface/templates/dashboard.php?pgs=alterar_cultura.php&id_cultura=$id_cultura&mensagem=".$mensagens[1]);
	

	}else{
		$erros[1]="Cultura não pode ser alterada";
		header("location:../interface/templates/dashboard.php?pgs=alterar_cultura.php&id_cultura=$id_cultura&erro=".$erros[1]);
		
	}
}else{
	$erros[1]="Campo não preenchido";
	header("location:../interface/templates/dashboard.php?pgs=alterar_cultura.php&id_cultura=$id_cultura&erro=".$erros[1]);
		
	
}


