<?php
include_once "../classes/Login.php";
include_once '../classes/Databases.php';
include_once 'valida.php';

$senhaAntiga = valida($_POST['senhaAntiga']);
$senha = valida($_POST['novaSenha']);
$confirmar_senha = valida($_POST['confirmar_nova_senha']);
$senhaCrip = sha1($senhaAntiga);
session_start();

if(!empty($senhaAntiga) || !empty($senha) || !empty($confirmar_senha)){
  
  session_start();
  $_SESSION['campos_alterar_senha'] = $_POST;

  if(!empty($senhaAntiga)){

    if(!empty($senha)){

      if (strlen($senha) >= 8)  {
        
       if (preg_match( '/[0-9]/' , $senha) ) {

          if (preg_match( '/[a-zA-Z]/' , $senha) ) {

            if (!empty($confirmar_senha)) {

              if ($senhaAntiga != $senha ) {

                $conexao = Databases::getConnection();
                $consulta="SELECT senha, usuario FROM usuario WHERE senha = '$senhaCrip'";
                $consultando = $conexao->query($consulta); 
                $resultado = $consultando->fetch(PDO::FETCH_OBJ);

                if ($resultado != false) {

                  if ($senha == $confirmar_senha  ){

                    $id_user = $_SESSION['logar']['id_usuario'];
                    $update =  new Login();
                    $update -> updateSenha($senha,$id_user);

                    unset( $_SESSION['campos_alterar_senha'] );

                  $mensagens[1] = "Sua senha foi alterada com sucesso";
                  header("location:../interface/templates/dashboard.php?pgs=alterar_senhas.php&mensagem=".$mensagens[1]);

                  }else {
                    $erros[1] = "Senha e confirmação não são iguais ";
                    header("location:../interface/templates/dashboard.php?pgs=alterar_senhas.php&erro=".$erros[1]);
                }  

              }else{
                $erros[1] = "Sua senha antiga está errada";
                header("location:../interface/templates/dashboard.php?pgs=alterar_senhas.php&erro=".$erros[1]);
              }

            }else {
              $erros[1] = "Sua senha atual não pode ser igual a nova";
              header("location:../interface/templates/dashboard.php?pgs=alterar_senhas.php&erro=".$erros[1]);
            }

          }else{
            $erros[1] = "Campo confirmar nova senha está vazio ";
            header("location:../interface/templates/dashboard.php?pgs=alterar_senhas.php&erro=".$erros[1]);
          }
        }else{
          $erros[0] = "A senha deve ser composta com letras";
          header("location:../interface/templates/dashboard.php?pgs=alterar_senhas.php&erro=".$erros[0]);
          }

      }else{
        $erros[0] = "A senha deve ser composta por letras e numeros";
        header("location:../interface/templates/dashboard.php?pgs=alterar_senhas.php&erro=".$erros[0]);
      }
     }else{
        $erros[0] = "A senha deve ser composta por no minimo 8 caracteres";
        header("location:../interface/templates/dashboard.php?pgs=alterar_senhas.php&erro=".$erros[0]);
      }    

    }else{
      $erros[1] = "Campo nova senha está vazio ";
      header("location:../interface/templates/dashboard.php?pgs=alterar_senhas.php&erro=".$erros[1]);
    }

  }else{
    $erros[1] = "Campo senha atual está vazio ";
    header("location:../interface/templates/dashboard.php?pgs=alterar_senhas.php&erro=".$erros[1]);
  }

}else{
  $erros[1] = "Campos estão vazios ";
  header("location:../interface/templates/dashboard.php?pgs=alterar_senhas.php&erro=".$erros[1]);
}





