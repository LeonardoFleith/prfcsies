<?php

date_default_timezone_set('America/Sao_Paulo');

include_once "../classes/Leitura.php";
include_once '../classes/Cultivo.php';

$cod_arduino = $_GET['idcont'];
$id = new Cultivo();
$id_cultivo = $id-> consultaID(); 

if ($cod_arduino != null) {
	
	$idbomba =$_GET['idbomba'];
	$status = $_GET['statusrele'];
	$temperatura = $_GET[ 'temp'];
	$umi_ar = $_GET['humi'];
	$umi_solo1 = $_GET['hig1'];
	$umi_solo2 = $_GET['hig2'];
	$umi_solo3 = $_GET['hig3'];
	$data = date('Y-m-d');
	$hora = date('H:i:s');

	$cadastrar_leitura = new Leitura();
	$cadastrar_leitura -> cadastraLeitura($temperatura, $data, $umi_solo1, $umi_solo2, $umi_solo3, $umi_ar, $hora, $cod_arduino, $status, $idbomba, $id_cultivo);

	}