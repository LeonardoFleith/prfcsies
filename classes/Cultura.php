<?php

include_once "Databases.php";

class Cultura {
	
	private $tipo_planta;
	private $status;
	private $id_user;
	private $nome_cientifico;
	


	public function cadastracultura($tipo_planta, $status, $id_user, $nome_cientifico ){        

		$conn = Databases::getConnection();


		$cadastrando= "INSERT INTO cultura (cod_usuario, tipo_planta, status_cultura, nome_cientifico) VALUES ('$id_user','$tipo_planta', '$status', '$nome_cientifico' )";

		$resultado = $conn->exec($cadastrando);

		return $resultado;
	}

	public function dadosCultura (){
		$conn = Databases::getConnection();
		$cultura=[];
		$consulta = $conn->query("SELECT tipo_planta, status_cultura, cod_usuario, nome_cientifico,id_cultura FROM cultura, usuario WHERE cod_usuario = id_usuario and status_cultura ='Desativado'");
		$cultura = $consulta->fetchAll(PDO::FETCH_ASSOC);
		return $cultura; 
	}

	public function consultaID_cultura($cod_cultura){
		$conn = Databases::getConnection();
		$consulta = $conn->query("SELECT id_cultura, cod_usuario FROM cultura WHERE id_cultura = '$cod_cultura'");
		$cultura = $consulta->fetch(PDO::FETCH_ASSOC);
		return $cultura; 
	}



	public function apresentaCultura($id_cultura){
		$conn = Databases::getConnection();
		$cultura=[];
		$consulta = $conn->query("SELECT tipo_planta, nome_cientifico, id_cultura FROM cultura WHERE id_cultura = '$id_cultura'");
		$cultura = $consulta->fetch(PDO::FETCH_ASSOC);
		return $cultura; 
	}


	public function retornaCultura($id_usuario){
		$conn = Databases::getConnection();
		$cultura = [];
		$consulta = $conn->query("SELECT tipo_planta, id_cultura FROM cultura WHERE status_cultura ='Ativado' and cod_usuario='$id_usuario'");
		$cultura = $consulta->fetchAll(PDO::FETCH_ASSOC);
        return $cultura;
	}

	public function retornaCultura_pag($id_usuario){
		$conn = Databases::getConnection();
		$consulta = $conn->query("SELECT id_cultura,tipo_planta, status_cultura, nome_cientifico, cod_usuario FROM cultura WHERE status_cultura = 'Ativado' and cod_usuario='$id_usuario'");
		$cultura = $consulta->fetchAll(PDO::FETCH_ASSOC);
		$tamanho = count($cultura);
        return $tamanho;
	}

	public function quantidade_paginas_cultura($quantidade,$id_usuario){
        $objeto = new Cultura();
        $tamanho =$objeto-> retornaCultura_pag($id_usuario);
        $n_paginas = $tamanho / $quantidade;
        $pg_arredondado = ceil($n_paginas);

        return $pg_arredondado;
    }

    public function paginacao_cultura($pagina_escolhida, $quantidade,$id_usuario){

        $pagina = $pagina_escolhida - 1;
        $inicio = $pagina * $quantidade;

        $conexao = Databases::getConnection();
        $busca = "SELECT id_cultura,tipo_planta, status_cultura, nome_cientifico, cod_usuario FROM cultura WHERE status_cultura='Ativado' and cod_usuario='$id_usuario'LIMIT $quantidade OFFSET $inicio;";
        $resultado = $conexao->query($busca);
        $retorno = $resultado->fetchAll(PDO::FETCH_ASSOC);
        
        $pagina_esc = new Cultura();
        $pg_arredondado = $pagina_esc-> quantidade_paginas_cultura($quantidade,$id_usuario);
        


        if ($pagina_escolhida<=$pg_arredondado) {
          while ($pagina_escolhida<=$pg_arredondado) {
            return $retorno;
          }
        }else{
            echo "";
        }                           
}

	public function updateCultura($novo_nome_cientifico, $novo_tipo_planta, $id_cultura){

		$conn = Databases::getConnection();
 		$up = $conn->exec("UPDATE cultura SET nome_cientifico = '$novo_nome_cientifico' , tipo_planta = '$novo_tipo_planta', status_cultura ='Desativado' WHERE cultura.id_cultura = '$id_cultura'");
 		return $up;

	}


	public function updateCultura_Ativado($id_cultura){
  	
  	$conn = Databases::getConnection();
 	$up = $conn->exec("UPDATE cultura SET status_cultura = 'Ativado' WHERE cultura.id_cultura = $id_cultura");
 	return $sql;
  	
	}

	public function updateCultura_Excluido($id_cultura){
  	
  	$conn = Databases::getConnection();
 	$up = $conn->exec("UPDATE cultura SET status_cultura = 'Excluido' WHERE cultura.id_cultura = $id_cultura");
 	return $sql;
  	
	}
		
}