<?php

include_once "Databases.php";


class Solo {
	
    private $tipo_solo;
    private $cap_campo;
    private $id_user;
    private $status;


    
    public function cadastrasolo($tipo_solo, $cap_campo,$id_user, $status){
 
    	$conn = Databases::getConnection();

		$cadastrando = "INSERT INTO solo (cod_usuario, tipo_solo, cap_de_campo, status_solo) VALUES ('$id_user','$tipo_solo','$cap_campo', '$status' )";

		$resultado = $conn->exec($cadastrando);

		return $resultado;

    }
 
    public function dadosSolo (){
		$conn = Databases::getConnection();
		$solo=[];
		$consulta = $conn->query("SELECT tipo_solo, status_solo, cod_usuario, cap_de_campo, id_solo FROM solo, usuario WHERE cod_usuario = id_usuario and status_solo='Desativado'");
		$solo = $consulta->fetchAll(PDO::FETCH_ASSOC);
		return $solo; 
	}

	public function consultaID_solo($cod_solo){
		$conn = Databases::getConnection();
		$solo=[];
		$consulta = $conn->query("SELECT cod_usuario,id_solo FROM solo WHERE id_solo = '$cod_solo'");
		$solo = $consulta->fetch(PDO::FETCH_ASSOC);
		return $solo; 

	}

  public function apresentaSolo($id_solo){
    $conn = Databases::getConnection();
    $solo = [];
    $consulta = $conn->query("SELECT tipo_solo, cap_de_campo, id_solo FROM solo WHERE id_solo = '$id_solo'");
    $solo = $consulta->fetch(PDO::FETCH_ASSOC);
    return $solo; 
  }

  public function retornaSolo($id_usuario){
    $conn = Databases::getConnection();
    $solo=[];
    $consulta = $conn->query("SELECT tipo_solo, id_solo FROM solo WHERE status_solo ='Ativado' and cod_usuario ='$id_usuario'");
    $solo = $consulta->fetchAll(PDO::FETCH_ASSOC);
        return $solo;
  }

	public function retornaSolo_pag($id_usuario){
		$conn = Databases::getConnection();
		$consulta = $conn->query("SELECT id_solo,cap_de_campo, status_solo, tipo_solo, cod_usuario FROM solo WHERE status_solo = 'Ativado' and cod_usuario ='$id_usuario'");
		$solo = $consulta->fetchAll(PDO::FETCH_ASSOC);
        $tamanho = count($solo);
        return $tamanho;
	}

	public function quantidade_paginas_solo($quantidade, $id_usuario){
        $objeto = new Solo();
        $tamanho =$objeto-> retornaSolo_pag($id_usuario);
        $n_paginas = $tamanho / $quantidade;
        $pg_arredondado = ceil($n_paginas);

        return $pg_arredondado;
    }

     public function paginacao_solo($pagina_escolhida, $quantidade, $id_usuario){

        $pagina = $pagina_escolhida - 1;
        $inicio = $pagina * $quantidade;

        $conexao = Databases::getConnection();
        $busca = "SELECT  id_solo,cap_de_campo, status_solo, tipo_solo, cod_usuario  FROM solo WHERE status_solo='Ativado' and cod_usuario ='$id_usuario' LIMIT $quantidade OFFSET $inicio;";
        $resultado = $conexao->query($busca);
        $retorno = $resultado->fetchAll(PDO::FETCH_ASSOC);
        
        $pagina_esc = new Solo();
        $pg_arredondado = $pagina_esc-> quantidade_paginas_solo($quantidade, $id_usuario);
        


        if ($pagina_escolhida<=$pg_arredondado) {
          while ($pagina_escolhida<=$pg_arredondado) {
            return $retorno;
          }
        }else{
            echo "";
        }                           
}

  public function updateSolo( $nova_cap_campo,$novo_tipo_solo, $id_solo){

    $conn = Databases::getConnection();
    $up = $conn->exec("UPDATE solo SET tipo_solo = '$novo_tipo_solo' , cap_de_campo = '$nova_cap_campo', status_solo = 'Desativado' WHERE solo.id_solo = '$id_solo'");
    return $up;
  }


	public function updateSolo_Ativado($id_solo){
  	
  $conn = Databases::getConnection();
 	$up = $conn->exec("UPDATE solo SET status_solo = 'Ativado' WHERE solo.id_solo = $id_solo");
 	return $up;
  	
	}

	public function updateSolo_Excluido($id_solo){
  	
  	$conn = Databases::getConnection();
 	$up = $conn->exec("UPDATE solo SET status_solo = 'Excluido' WHERE solo.id_solo = $id_solo");
 	return $up;
  	
	}

}
