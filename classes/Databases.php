<?php

class Databases {

   public static function getConnection(){
    
        try{
            $conn= new PDO("mysql:host=localhost;dbname=prfc_sies",'root','');
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            return $conn;
        }  
        catch (PDOException$erro){
            echo 'Conexão falhou: '. $erro->getMessage();
        }
    }
}
