<?php

include_once "Databases.php";

class Cultivo {
   
	private $nome_cultivo;
	private $id_cultura;
	private $id_solo;
  private $status_cultivo;
   	

	public function cadastraCultivo($nome_cultivo, $id_cultura, $id_solo){

		$conexao = Databases::getConnection();
	
		$cadastrando = "INSERT INTO cultivo(nome_cultivo, cod_solo, cod_cultura, status_cultivo) VALUES ('$nome_cultivo', '$id_solo', '$id_cultura','Desativado')";

    	$resultado = $conexao->exec($cadastrando);

        return $resultado;
	}

  public function consultaStatus($id_usuario){
    $conexao = Databases::getConnection();
    $consulta = $conexao -> query("SELECT status_cultivo FROM cultivo,solo,cultura where cod_solo = id_solo and cod_cultura = id_cultura and solo.cod_usuario = $id_usuario and cultura.cod_usuario = $id_usuario and status_cultivo='Ativado'");
    $status = $consulta -> fetch(PDO::FETCH_ASSOC);
    
    if (empty($status))
      return null;
    
    return $status;
  
  }

  public function consultaID(){
    $conexao = Databases::getConnection();
    $consulta = $conexao -> query("SELECT id_cultivo FROM cultivo where status_cultivo='Ativado'");
    $id = $consulta -> fetch(PDO::FETCH_ASSOC);
    return $id;
  }
  
  public function apresentaCultivo($id_cultivo){
    $conexao = Databases::getConnection();
    $cadastrados =[];
    $consulta = $conexao->query("SELECT nome_cultivo, cod_solo, cod_cultura, tipo_planta,tipo_solo FROM cultivo,solo,cultura where cod_solo = id_solo and cod_cultura = id_cultura and id_cultivo = '$id_cultivo'");
    $cadastrados = $consulta->fetch(PDO::FETCH_ASSOC);
    return $cadastrados;
  }

  public function retornaCultivo($id_usuario){
    $conexao = Databases::getConnection();
    $cadastrados =[];
    $consulta = $conexao->query("SELECT nome_cultivo, id_cultivo FROM cultivo,solo,cultura where cod_solo = id_solo and cod_cultura = id_cultura and solo.cod_usuario=$id_usuario and cultura.cod_usuario = $id_usuario and status_cultivo != 'Excluido'");
    $cadastrados = $consulta->fetchAll(PDO::FETCH_ASSOC);
    
        return $cadastrados;
  }

	public function retornaCultivo_pag($id_usuario){
		$conexao = Databases::getConnection();
    $busca = "SELECT cap_de_campo, status_solo, tipo_solo, tipo_planta, status_cultura, nome_cientifico, nome_cultivo, status_cultivo, id_cultivo FROM cultivo,solo,cultura where cod_solo = id_solo and cod_cultura = id_cultura and solo.cod_usuario=$id_usuario and cultura.cod_usuario = $id_usuario and status_cultivo != 'Excluido' and solo.status_solo = 'Ativado' and cultura.status_cultura = 'Ativado'";
		$consulta = $conexao->query($busca);
		$cadastrados = $consulta->fetchAll(PDO::FETCH_ASSOC);
		$tamanho = count($cadastrados);
        return $tamanho;
	}

	public function quantidade_paginas_cultivo($quantidade,$id_usuario){
        $objeto = new Cultivo();
        $tamanho =$objeto-> retornaCultivo_pag($id_usuario);
        $n_paginas = $tamanho / $quantidade;
        $pg_arredondado = ceil($n_paginas);

        return $pg_arredondado;
    }

    public function paginacao_cultivo($pagina_escolhida, $quantidade,$id_usuario){

        $pagina = $pagina_escolhida - 1;
        $inicio = $pagina * $quantidade;

        $conexao = Databases::getConnection();
        $busca = "SELECT cap_de_campo, status_solo, tipo_solo, tipo_planta, status_cultura, nome_cientifico, nome_cultivo, status_cultivo, id_cultivo FROM cultivo,solo,cultura where cod_solo = id_solo and cod_cultura = id_cultura and solo.cod_usuario=$id_usuario and cultura.cod_usuario = $id_usuario and status_cultivo != 'Excluido' and solo.status_solo = 'Ativado' and cultura.status_cultura = 'Ativado' LIMIT $quantidade OFFSET $inicio;";
        $resultado = $conexao->query($busca);
        $retorno = $resultado->fetchAll(PDO::FETCH_ASSOC);
        
        $pagina_esc = new Cultivo();
        $pg_arredondado = $pagina_esc-> quantidade_paginas_cultivo($quantidade,$id_usuario);
        


        if ($pagina_escolhida<=$pg_arredondado) {
          while ($pagina_escolhida<=$pg_arredondado) {
            return $retorno;
          }
        }else{
            echo "";
        }                           
}

	public function updateCultivo($novo_nome_cultivo, $id_cultivo, $novo_id_solo, 
  $novo_id_cultura){

    $conn = Databases::getConnection();
    $up = $conn->exec("UPDATE cultivo SET nome_cultivo = '$novo_nome_cultivo' , cod_solo = '$novo_id_solo' , cod_cultura = '$novo_id_cultura' WHERE cultivo.id_cultivo = '$id_cultivo'");
    return $up;

  }

	public function updateCultivo_Excluido($id_cultivo){
  	
  		$conn = Databases::getConnection();
 		$up = $conn->exec("UPDATE cultivo SET status_cultivo = 'Excluido' WHERE cultivo.id_cultivo = $id_cultivo");
 		return $sql;
  	
	}

	public function updateCultivo_Ativado($id_cultivo){
  	
  	$conn = Databases::getConnection();
 		$up = $conn->exec("UPDATE cultivo SET status_cultivo = 'Ativado' WHERE cultivo.id_cultivo = $id_cultivo");
 		return $sql;
  	
	}

	public function updateCultivo_Desativado($id_cultivo){
  	
  		$conn = Databases::getConnection();
 		$up = $conn->exec("UPDATE cultivo SET status_cultivo = 'Desativado' WHERE cultivo.id_cultivo = $id_cultivo");
 		return $sql;
  	
	}

	
}
