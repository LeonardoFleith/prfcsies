
<html lang="en" class="no-js">
    <head>
        <meta charset="UTF-8">
        <title>SIES - Sistema de Irrigação Eco-Sustentável</title>
         <link rel="shortcut icon" href="img/folha.png">
        <!-- LINKS -->
        <link rel="stylesheet" href="bootstrap-3.3.7/dist/css/bootstrap.min.css">
        <link rel="stylesheet" href="css/animate.css">
        <link rel="stylesheet" href="css/font-awesome.min.css">
        <link rel="stylesheet" href="fonts/font.css">
        <link rel="stylesheet" href="css/styles.css">
    </head>

    <body>
        <header>
            <!-- NAV -->
             <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#oi">
                    <span class="fa fa-bars fa-lg"></span>
                </button>
                <div class="container">
                    <a class="navbar-brand" href="index.php">
                        <img src="img/logo.png" alt="" class="logo">
                    </a>
                    <section class="collapse navbar-collapse" id="oi">
                        <ul class="nav navbar-nav navbar-right navbar-collapse collapse in">
                            <li><a href="#sobre">Sobre</a></li>
                            <li><a href="templates/entrar.php">Entrar</a></li>
                        </ul>
                </section>
                </div>
            </nav>
            <!-- FIM NAV-->
           <article class="container">            
                <section class= "col-md-8 col-md-offset-2">
                    <section class="section-heading  sp-effect3 titulo">
                        <h1>Sistema de Irrigação Eco Sustentável</h1>
                    </section>
                </section>
            </article>

        </header>


            <!--SOBRE-->
            <section class="sobre" id="sobre">
                <section class="container">
                    <section class="section-heading  sp-effect3">
                        <h1>Sobre nós</h1>
                        <div class="divider"></div>
                        <p>Esse projeto consiste em um software de irrigação que determina a quantidade necessária de água para a plantação com a ajuda de sensores, tendo como base a umidade do solo, do ar e a temperatura do ambiente, focando sempre na economia de água. Esse sistema traz um melhor rendimento as plantações sem a necessidade de um gasto elevado de água, trazendo assim um maior lucro a você produtor.</p>
                    </section>
                  
                </section>
            </section>
            <!--FIM SOBRE-->

            <!--FOOTER-->
            <footer>
                <section class="container">
                    <a href="#" class=" sp-effect3">
                        <img src="img/logo.png" alt="" class="logo">
                    </a>
                    <div class="rights">
                        <p>&raquo; Izabelle Maria Gelain, Julia Maria Nogueira e Leonardo Vitor Fleith Faria - 3InfoI1 &laquo;</p>       
                    </div>
                </section>
            </footer>
            <!--FIM FOOTER-->
        </div>

        <script src="js/jquery-1.11.1.min.js"></script>
        <script src="bootstrap-3.3.7/dist/js/bootstrap.min.js"></script>
        <script src="js/slick.min.js"></script>
        <script src="js/placeholdem.min.js"></script>
        <script src="js/jquery.themepunch.revolution.min.js"></script>
        <script src="js/waypoints.min.js"></script>
        <script src="js/scripts.js"></script>
        <script>
            $(document).ready(function() {
                appMaster.preLoader();
            });
        </script>
    </body>

</html>
