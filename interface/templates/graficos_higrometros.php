<?php 

include_once "../../classes/databases.php";

function consultaPorDia(){
  
    $id = $_GET['id_cultivo'];
    $conexao = Databases::getConnection();
    $consultando = "SELECT umidade_solo1, umidade_solo2, umidade_solo3, data_leitura, hora FROM leituras where cod_cultivo = ".$id." and  data_leitura > current_date -1"; /*current_date -1*/
  
    $resultado = $conexao->query($consultando);

    return $resultado;
}


function consultaPorSemana(){
    
    $id = $_GET['id_cultivo'];  
    $conexao = Databases::getConnection();
    $consultando = "SELECT umidade_solo1, umidade_solo2, umidade_solo3, data_leitura, hora FROM leituras where cod_cultivo = ".$id ." and  data_leitura > current_date -7"; /*current_date -7*/

    $resultado = $conexao->query($consultando);

    return $resultado;
}


function consultaPorMes(){
        $id = $_GET['id_cultivo'];
        $conexao = Databases::getConnection();
        $consultando = "SELECT umidade_solo1, umidade_solo2, umidade_solo3, data_leitura, hora FROM leituras where cod_cultivo = ". $id ." and  data_leitura > current_date -30"; /*current_date -30*/
        
        $resultado = $conexao->query($consultando);

    return $resultado;
  }

?>

  <script src="../js/jquery-1.11.1.min.js" type="text/javascript"></script>
  <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>


  <div class="col-md-11 padding-col-2">

    <div class="nav cadastrados">

      <ul id="tabsCadastrados"  class="nav nav-tabs nav-justified">
        <li class="active"><a aria-controls="solo" role="tab" data-toggle="tab" href="#dia">Dia</a></li>
        <li ><a aria-controls="cultura" role="tab" data-toggle="tab" href="#semana">Semana</a></li>
        <li><a aria-controls="cultivo" role="tab" data-toggle="tab" href="#mes">Mês</a></li>
      </ul>

      <div class="tab-content lista1">
        <div role="tabpanel" class="tab-pane fade in active" id="dia">
          <div id="chart_day" style="width: auto; height: 450px"></div>
        </div>
        
        <div role="tabpanel" class="tab-pane fade in" id="semana">   
            <div id="chart_week" style="width: 100%; height: 450px"></div>
       </div>
        
        <div role="tabpanel" class="tab-pane fade in" id="mes">  
         <div id="chart_month" style="width: 100%; height: 450px"></div>
       </div>

     </div>
   </div>

  <script type="text/javascript">
    google.charts.load('current', {'packages':['corechart']});
    
    google.charts.setOnLoadCallback(drawChartDay);

    function drawChartDay() {
      var dataDay = google.visualization.arrayToDataTable([
        ['Hora', 'Umidade sensor 1', 'Umidade sensor 2', 'Umidade sensor 3',],
        <?php
        $dados = consultaPorDia();
        foreach($dados as $dado) {?>  

          [ '<?php echo $dado['hora'];?>', <?php echo $dado['umidade_solo1'];?>, <?php echo $dado['umidade_solo2'];?>, <?php echo $dado['umidade_solo3'];?>,],
          <?php
        }?>
        ]);

      var optionsDay = {    

        title: 'Gráfico por dia',
        legend: { position: 'bottom'},
        hAxis: {    
        },
        vAxis: {  
          logScale: true,
          scaleType: 'log',
          minValue: 0
        }



      };

      var chart = new google.visualization.LineChart(document.getElementById('chart_day'));

      chart.draw(dataDay, optionsDay);
    }
  

    function drawChartWeek() {
      var data = google.visualization.arrayToDataTable([
        ['Hora', 'Umidade sensor 1', 'Umidade sensor 2', 'Umidade sensor 3',],
        <?php
        $dados = consultaPorSemana();
        foreach($dados as $dado) {?>  

          [ '<?php echo $dado['hora'];?>', <?php echo $dado['umidade_solo1'];?>, <?php echo $dado['umidade_solo2'];?>, <?php echo $dado['umidade_solo3'];?>,],
          <?php
        }?>
        ]);

      var options = {    

        title: 'Gráfico por semana',
        legend: { position: 'bottom'},
        hAxis: {    
        },
        vAxis: {  
          logScale: true,
          scaleType: 'log',
          minValue: 0
        }



      };

      var chart = new google.visualization.LineChart(document.getElementById('chart_week'));

      chart.draw(data, options);

    }

    function drawChartMonth() {
        var data = google.visualization.arrayToDataTable([
          ['Hora', 'Umidade sensor 1', 'Umidade sensor 2', 'Umidade sensor 3',],
          <?php 
          $dados = consultaPorMes();
          foreach($dados as $dado) {?>  
                              
          [ '<?php echo $dado['hora'];?>', <?php echo $dado['umidade_solo1'];?>, <?php echo $dado['umidade_solo2'];?>, <?php echo $dado['umidade_solo3'];?>,],
       <?php
     }?>
        ]);

        var options = {    

                        title: 'Gráfico por mes',
                        legend: { position: 'bottom'},
                        hAxis: {    
                              },
                        vAxis: {  
                            logScale: true,
                            scaleType: 'log',
                            minValue: 0
                                }

                             

        };

        var chart = new google.visualization.LineChart(document.getElementById('chart_month'));

        chart.draw(data, options);
    }

    $('#tabsCadastrados li a').on( "click", function(){

        var tab = $(this).attr('href');

        if (tab == '#semana') {
          google.charts.setOnLoadCallback(drawChartWeek);
        } 

        if(tab == '#mes'){
          google.charts.setOnLoadCallback(drawChartMonth);
        }

        $(this).click();

    });
  </script>



