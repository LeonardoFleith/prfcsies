<?php 
    include_once '../../classes/Solo.php'; 
 
 if( $_SESSION['logar']['cod_tipo_user'] == 1){
?>
	<section class="lista">
    	<?php
        	$solo = new Solo();
        	$solos= $solo -> dadosSolo ();?>
                <div class="section-heading  dois">
                    <h1>Solicitação Cadastro de Solo</h1>
                    <div class="divider"></div>
                </div>

        	<?php 

         if ( !empty($solos)) {

            foreach ($solos as $solo) {?>
             
            	<section class="solicitado col-md-6">
                	<section class="text-holder">
                    	<section class="feed-title">
                        	<h4><?= $solo['tipo_solo'];?> 
                        	</h4>
                   	 	</section>
                    <section class="feed-description">
                       <p> Capacidade de Campo: <?= $solo['cap_de_campo'];?> 
                        <button class="btn btn-primary btn-lg botao exclui">
                         <a href="../../controladores/update_solo_excluir_adm.php?id_solo=<?= $solo['id_solo'];?>"> Excluir 
                           </a> 
                       
                           
                        </button>
                           <button class=" btn btn-primary btn-lg botao ativa">
                            <a href="../../controladores/update_solo_ativar.php?id_solo=<?= $solo['id_solo'];?>"> Ativar </a> 
                       
                           
                        </button></p>
                    </section>
                	</section>
            	</section>
        <?php }  
     }  

    else{ ?>
            <section class="text-holder">
                    <section class="feed-title">
                        <h4>Nenhum solo solicitado </h4>
                    </section>
                </section> <?php

    } 
    ?>
</section>
<?php
}else{
	echo("<script type='text/javascript'> alert( 'Você não pode acessar está pagina :(' );
            location.href='../templates/dashboard.php?pos=1&pgs=cadastro_solo.php';</script>");
	}
