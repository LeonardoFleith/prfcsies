<section class="doublediagonal">
    <div class="container logado">
        <div class="col-md-9  col-sm-9 padding-col">
            <div class="section-heading dois">
                <h1>Relatório - Higrometros</h1>
                <div class="divider"></div>
                <p>Selecione um cultivo</p>
            </div>
            <form method="get" action="../../controladores/controla_graficos_higro.php" role="form col-sm-2">
                 <?php if(isset($_GET['erro'])) :?>

                    <div class="alert alert-danger alert-dismissible erroentra" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <strong> Atenção!</strong>  <?= @$_GET['erro'] ?>
                    </div>

                <?php endif ?>
                <div class="form-group">
                    <label>Nome do cultivo</label>
                    <select name="id_cultivo" class="form-control">
                        <option class=" form-control" value="">Selecione...</option>  
                        <?php
                        $id_usuario = $_SESSION['logar']['id_usuario'];
                        include_once "../../classes/Cultivo.php";
                        $nome_cultivo = new Cultivo();
                        $nome_cultivos = $nome_cultivo -> retornaCultivo($id_usuario);
                        foreach ($nome_cultivos as $nome_cultivo) {?>  
                        <option class=" form-control" value="<?=$nome_cultivo['id_cultivo'];?>"><?=$nome_cultivo['nome_cultivo']; ?>
                        </option>
                        <?php } 
                        ?>
                    </select>
                </div>
                <button class="btn btn-primary btn-lg">Enviar</button>       
            </form>   
        </div>
    <div>
</section>