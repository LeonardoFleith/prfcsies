
<section class="doublediagonal">
    <div class="container logado">
        <div class=" col-md-9  col-sm-9 padding-col">
            <div class="section-heading  dois">
                <h1>Cadastro de Solo</h1>
            <div class="divider"></div>
            </div>
            <form method="post" action="../../controladores/cad_solo.php" role="form col-sm-2">
          
             <?php if(isset($_GET['erro'])) :?>

                        <div class="alert alert-danger alert-dismissible erroentra" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <strong> Atenção!</strong>  <?= @htmlspecialchars($_GET['erro']); ?>
                        </div>

                        <?php endif ?>
                        <?php if(isset($_GET['mensagem'])) :?>

                    <div class="alert alert-success alert-dismissible erroentra" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <strong> Parabéns!</strong>  <?php echo @htmlspecialchars($_GET['mensagem']);?>
                </div>
                <?php endif ?>
                <div class="form-group">
                    <label>Tipo do Solo</label>
                    <input type="text" class="form-control" placeholder="Ex.: Substrato Comercial" name="tipo_solo" value="<?= @$_SESSION['campos_solo']['tipo_solo'] ?>" >
                </div>
                <div class="form-group">
                    <label>Capacidade de campo</label>
                    <input type="number" class="form-control" placeholder="Ex.: 130" name="cap_campo" value="<?= @$_SESSION['campos_solo']['cap_campo'] ?>">
                </div>                    
                <?php
                    $tipo_user = $_SESSION['logar']['cod_tipo_user'];
                    $id_user = $_SESSION['logar']['id_usuario'];
                    echo ('
                        <input type="hidden" name="tipo_user" value="'.$tipo_user.'" class="form-control">
                        <input type="hidden" name="id_user" value="'.$id_user.'" class="form-control">
                    '); 
                ?>
                <button class="btn btn-primary btn-lg">Enviar</button>        
            </form>   
        </div>
    <div>
</section>