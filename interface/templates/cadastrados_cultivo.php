<div class="col-md-11 padding-col-2">
  <div class="section-heading  dois">
    <h1>Cadastros Efetuados</h1>
  </div>
  <div class="nav cadastrados">
  
    <ul id="" class="nav nav-tabs nav-justified">
      <li><a href="?pgs=cadastrados_solo.php&pg=1">Solo</a></li>
      <li><a href="?pgs=cadastrados_cultura.php&pg=1">Cultura</a></li>
      <li class="active"><a href="?pgs=cadastrados_cultivo.php&pg=1">Cultivo</a></li>
    </ul>

    <div class="tab-content">
      <div role="tabpanel"  class="tab-pane fade in active" id="cultivo">

<section class="lista lista1">
    <?php
    include_once '../../classes/Cultivo.php';
   
    function faz_botao($status,$id){
      $id_usuario = $_SESSION['logar']['id_usuario'];
        if($status == "Ativado"){
            return'<button class=" btn btn-primary btn-lg botao exclui">
                        <a href="../../controladores/update_cultivo_desativar.php?id_cultivo='.$id.'"> Desativar </a> 
                    </button>';
        } else{ 
            return '<button class="  btn btn-primary btn-lg botao ativa ">
                        <a href="../../controladores/update_cultivo_ativar.php?id_cultivo='.$id.'&id_usuario='.$id_usuario.'"> Ativar </a> 
                    </button>';
        }
    }

    $id_usuario = $_SESSION['logar']['id_usuario'];
  
    $pg_atual = $_GET['pg'];
    $quantidade = 3;
    $cadastrado = new Cultivo();
    $cadastrados= $cadastrado ->paginacao_cultivo($pg_atual, $quantidade, $id_usuario);

    $n_pagina = new Cultivo();
    $limite = $n_pagina->quantidade_paginas_cultivo($quantidade, $id_usuario);
        
        $anterior = $pg_atual -1;
        $proxima = $pg_atual + 1;
        if ($pg_atual=1) {
          $anterior = $pg_atual;
        }
if ($_GET['pg'] > $limite) {
          ?>

    <section class="text-holder">
      <section class="feed-title">
          <h4>Não há mais nenhum cultivo cadastrado </h4>
      </section>
    </section> 
    <p></p>
    <a href='?pgs=cadastrados_cultivo.php&pg=<?=$anterior;?>' class="">Voltar para a inicial</>
    <?php
      exit();
}

if ( !empty($cadastrados)) {
    foreach ($cadastrados as $cadastrado) {?>
    <section class="solicitado col-md-6">
        <section class="text-holder">
            <section class="feed-title">
                <h4><?= $cadastrado['nome_cultivo'];?> 
                </h4>
                <section class="feed-description">
                    <div class="col-md-3">
                        <p>Nome solo: <?= $cadastrado['tipo_solo'];?> </p>

                        <p>Nome cultura: <?= $cadastrado['tipo_planta'];?> </p>
                    </div>
                    <div class="col-md-9">
                        <p class="cap_campo">Capacidade de campo: <?= $cadastrado['cap_de_campo'];?> </p>
                        <div class="botoes">
                          <button class=" btn btn-primary btn-lg botao exclui">
                              <a href="../../controladores/update_cultivo_excluir.php?id_cultivo=<?= $cadastrado['id_cultivo'];?>"> Excluir </a>
                          </button>
                          <button class="btn btn-primary btn-lg botao edita">
                              <a href="?pgs=alterar_cultivo.php&id_cultivo=<?=$cadastrado['id_cultivo'];?>"> Editar </a>
                          </button>
                          <?=faz_botao($cadastrado['status_cultivo'], $cadastrado['id_cultivo']);?>
                        </div>
                    </div>
                   
                    
                </section>
            </section>
        </section>
    </section> 
    <?php }}  
          
    ?>
<nav aria-label="...">
  <ul class="pager">
    <li class="previous"><a href="?pgs=cadastrados_cultivo.php&pg=<?=$anterior?>"><span aria-hidden="true">&larr;</span> Anterior</a></li>
    <li class="next"><a href="?pgs=cadastrados_cultivo.php&pg=<?=$proxima?>">Próximo <span aria-hidden="true">&rarr;</span></a></li>
  </ul>
</nav>
</section>
 </div>

    </div>
  </div>

</div>

