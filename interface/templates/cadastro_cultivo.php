
<section class="doublediagonal">
    <div class="container logado">
        <div class="col-md-9  col-sm-9 padding-col">
            <div class="section-heading dois">
                <h1>Cadastro de Cultivo</h1>
                <div class="divider"></div>
            </div>
            <form method="post" action="../../controladores/cad_cultivo.php" role="form col-sm-2">
                 <?php if(isset($_GET['erro'])) :?>

                        <div class="alert alert-danger alert-dismissible erroentra" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <strong> Atenção!</strong>  <?=@htmlspecialchars($_GET['erro']);?>
                        </div>

                        <?php endif ?>
                        <?php if(isset($_GET['mensagem'])) :?>

                    <div class="alert alert-success alert-dismissible erroentra" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <strong> Parabéns!</strong>  <?php echo @htmlspecialchars($_GET['mensagem']);?>
                </div>
                <?php endif ?>
                <div class="form-group">
                    <label>Nome do Cultivo</label>
                    <input type="text" name="nome_cultivo" class="form-control" placeholder="Ex.: Cultivo de Alface" value="<?= @$_SESSION['campos_cultivo']['nome_cultivo'] ?>">
                </div>
                <div class="form-group">
                    <label>Nome da Cultura</label>
                    <select name="id_cultura" class="form-control" >
                        <option class=" form-control" required >Selecione...</option>  
                        <?php
                        $id_usuario = $_SESSION['logar']['id_usuario'];
                        include_once "../../classes/Cultura.php";
                        $tipo_planta = new Cultura();
                        $tipo_plantas = $tipo_planta -> retornaCultura($id_usuario);
                        foreach ($tipo_plantas as $tipo_planta) {?>  
                        <option class=" form-control" value="<?= $tipo_planta['id_cultura'];?>"><?=$tipo_planta['tipo_planta']; ?>
                        </option>
                        <?php } 
                        ?>
                    </select>
                </div>
                <div class="form-group">
                    <label>Nome do Solo</label>
                    <select name="id_solo" class="form-control" >
                        <option class=" form-control" required >Selecione... </option>
                        <?php
                        include_once "../../classes/Solo.php";
                        $tipo_solo = new Solo();
                        $tipo_solos = $tipo_solo -> retornaSolo($id_usuario);
                        foreach ($tipo_solos as $tipo_solo) {?>
                        <option class=" form-control" value="<?=$tipo_solo['id_solo'];?>"><?=$tipo_solo['tipo_solo'];?></option>
                        <?php } 
                        ?>
                    </select>
                </div>
                <?php
                    $id_user = $_SESSION['logar']['id_usuario'];
                    echo ('
                    <input type="hidden" name="id_user" value="'.$id_user.'" class="form-control">
                    '); 
                ?>
                <button class="btn btn-primary btn-lg">Enviar</button>       
            </form>   
        </div>
    <div>
</section>