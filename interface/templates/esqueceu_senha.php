
<html lang="en" class="no-js">

<head>
    <meta charset="UTF-8">
    <title>SIES - Sistema de Irrigação Eco-Sustentável</title>
    <!-- LINKS -->
    <link rel="stylesheet" href="../bootstrap-3.3.7/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/animate.css">
    <link rel="stylesheet" href="../css/font-awesome.min.css">
    <link rel="stylesheet" href="../fonts/font.css">
    <link rel="stylesheet" href="../css/styles.css">
    <!-- LINKS -->
</head>

<body>
    <?php
        require("menu.php") 
    ?>
    <!--ENTRAR-->
    <section id="Entrar" class="doublediagonal">
        <div class="container">
            <div class="section-heading ">
                <h1>Esqueceu sua senha?</h1>
            <div class="divider"></div>
            </div>
            <div class="row">
                <div class="col-md-6 ">
                    <div class="row">
                        <div class="col-md-8 col-md-offset-8 col-sm-7 col-sm-offset-3 col-xs-8 col-xs-offset-2   ">
                            <form role="form" action="../../controladores/esqueceu_senha.php" method="post">
                             <?php if(isset($_GET['erro'])) :?>

                        <div class="alert alert-danger alert-dismissible erroentra" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <strong> Atenção!</strong>  <?= @htmlspecialchars($_GET['erro']); ?>
                        </div>

                        <?php endif ?>
                        <?php if(isset($_GET['mensagem'])) :?>

                    <div class="alert alert-success alert-dismissible erroentra" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <strong> Parabéns!</strong>  <?php echo @htmlspecialchars($_GET['mensagem']);?>
                </div>
                <?php endif ?>
                                <div class="form-group">
                                    <label>Email</label>
                                    <input type="email" class="form-control" placeholder="Ex.: usuario@gmail.com" name="email">
                                </div> 
                                <div class="media">
                                    <div class="media-body">
                                        <h4 class="media-heading">
                                        
                                             <button type="submit" class="btn btn-primary btn-lg">Enviar</button>
                                        </h4>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--FIM ENTRAR --> 
    <?php
        require("footer.php") 
    ?>

    <script src="../js/jquery-1.11.1.min.js"></script>
    <script src="../bootstrap-3.3.7/dist/js/bootstrap.min.js"></script>
    <script src="../js/slick.min.js"></script>
    <script src="../js/placeholdem.min.js"></script>
    <script src="../js/waypoints.min.js"></script>
    <script src="../js/scripts.js"></script>
    <script>
        $(document).ready(function() {
            appMaster.preLoader();
        });
    </script>
</body>
</html>
