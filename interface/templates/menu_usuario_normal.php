<div class="collapse navbar-collapse" id="barra-lateral">
    <div id="sidebar-wrapper">
        <ul class="sidebar-nav">
            <li class="logo">
                <a href="#">
                    <img src="../img/logo.png" >
                </a>
            </li>
            <li>
                <a href="{menu1}">
                    Cadastro de Cultura
                </a>
            </li>
            <li>
                <a href="{menu2}">
                    Cadastro de Solo
                </a>
            </li>
            <li>
                <a href="{menu3}">
                    Cadastro de Cultivo
                </a>

            </li>    
            <li><a href="">
                    Relatórios
                </a>
                
                <ul class="interno">
                    <li><a href="{menu8}">Higrometros</a></li>
                    <li><a href="{menu9}">Ambiente</a></li> 
                </ul>
            </li> 
            <li>
                <a href="{menu6}">                        
                     Cadastros Efetuados
                </a>
            </li>
        </ul>
    </div>
</div>

<div id="menu-mobile" class="escondido">
    <div id="sidebar-wrapper">
        <ul class="sidebar-nav" >
            <li class="logo">
                <a href="#">
                    <img src="../img/logo.png" >
                </a>
            </li>
            <li>
                <a href="{menu1}">
                    Cadastro de Cultura
                </a>
            </li>
            <li>
                <a href="{menu2}">
                    Cadastro de Solo
                </a>
            </li>
            <li>
                <a href="{menu3}">
                    Cadastro de Cultivo
                </a>
            </li>    
            <li>
                <a href="">
                    Relatórios
                </a>
                <ul class="interno">
                    <li><a href="{menu8}">Higrometros</a></li>
                    <li><a href="{menu9}">Ambiente</a></li> 

                </ul>
            </a>
        </li>
        <li>
            <a href="{menu6}">                        
                Cadastros Efetuados
            </a>
        </li>
    </ul>
</div>
</div>