
<section class="doublediagonal">
    <div class="container logado">
        <div class="col-md-9  col-sm-9 padding-col">
            <div class="section-heading  dois">
                <h1>Alterar Senha</h1>

                <div class="divider"></div>
            </div>
            <form method="post" action="../../controladores/trocar_senha.php" role="form col-sm-2">
             <?php if(isset($_GET['erro'])) :?>

                        <div class="alert alert-danger alert-dismissible erroentra" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <strong> Atenção!</strong>  <?= @htmlspecialchars($_GET['erro']); ?>
                        </div>

                        <?php endif ?>
                        <?php if(isset($_GET['mensagem'])) :?>

                    <div class="alert alert-success alert-dismissible erroentra" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <strong> Parabéns!</strong>  <?php echo @htmlspecialchars($_GET['mensagem']);?>
                </div>
                <?php endif ?>
                <div class="form-group">
                    <label>Senha atual</label>
                    <input type="password" class="form-control" placeholder="Senha Atual" name="senhaAntiga" value="<?= @$_SESSION['campos_alterar_senha']['senhaAntiga'] ?>">
                    <label class="label label1">A nova senha deve ser diferente da atual**</label>
                </div>   
                <div class="form-group">
                    <label>Nova senha</label>
                    <input type="password" class="form-control" placeholder="Nova senha" name="novaSenha"value="<?= @$_SESSION['campos_alterar_senha']['novaSenha'] ?>" >
                    <label class="label label1">Min. 8 caracteres, letras e números.**</label>
                </div>
                <div class="form-group">
                    <label>Confirmar nova senha</label>
                    <input type="password" class="form-control" placeholder="Confirmar nova senha" name="confirmar_nova_senha" value="<?= @$_SESSION['campos_alterar_senha']['confirmar_nova_senha'] ?>" >
                </div> 
                <button class="btn btn-primary btn-lg">Enviar</button>        
            </form>   
        </div>
    <div>
</section>