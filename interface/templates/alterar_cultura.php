<?php
 $id_cultura = @htmlspecialchars($_GET['id_cultura']);
 
?>
<section class="doublediagonal">
    <div class="container logado">
        <div class="col-md-9  col-sm-9 padding-col">
            <div class="section-heading dois">
                <h1>Editar Cultura</h1>
                <div class="divider"></div>
            </div>
            <form method="post" action="../../controladores/update_cultura_editar.php" role="form col-sm-2">
                  <?php if(isset($_GET['erro'])) :?>

                        <div class="alert alert-danger alert-dismissible erroentra" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <strong> Atenção!</strong>  <?=@htmlspecialchars($_GET['erro']); ?>
                        </div>

                        <?php endif ?>
                        <?php if(isset($_GET['mensagem'])) :?>

                    <div class="alert alert-success alert-dismissible erroentra" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <strong> Parabéns!</strong>  <?php echo @htmlspecialchars($_GET['mensagem']);?>
                </div>
                <?php endif ?>

            <?php include_once "../../classes/Cultura.php";

            $cultura = new Cultura();
            $exibicao = $cultura -> apresentaCultura($id_cultura); 
            ?>
            
                <div class="form-group">
                    <label>Tipo de Planta</label>
                    <input type="text" name="novo_tipo_planta" class="form-control" value="<?=$exibicao['tipo_planta'];?>">
                    </div>
                      <div class="form-group">
                    <label> Nome científico</label>
                     <input type="text" name="novo_nome_cientifico" class="form-control" value="<?=$exibicao['nome_cientifico'];?>">   
                </div>
                 <input type="hidden" name="id_cultura" value="<?=$id_cultura?>" class="form-control">
                 <button type="submit" class="btn btn-primary btn-lg">Editar</button>        
            </form>   
        </div>
    <div>
</section>