<?php session_start(); ?>
<html lang="en" class="no-js">

<head>
    <meta charset="UTF-8">
    <title>SIES - Sistema de Irrigação Eco-Sustentável</title>
    <!-- LINKS -->
    <link rel="stylesheet" href="../bootstrap-3.3.7/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/animate.css">
    <link rel="stylesheet" href="../css/font-awesome.min.css">
    <link rel="stylesheet" href="../fonts/font.css">
    <link rel="stylesheet" href="../css/styles.css">
    <!-- LINKS -->
</head>

<body>
    <?php
        require("menu.php") 
    ?>
    <!--CADASTRE-SE-->
    <section id="Entrar" class="doublediagonal">
        <div class="container">
            <div class="section-heading ">
                <h1>Cadastre-se</h1>
            <div class="divider"></div>
            </div>
            <div class="row">
                <div class="col-md-6 ">
                    <div class="row">
                        <div class="col-md-8 col-md-offset-8 col-sm-7 col-sm-offset-3 col-xs-8 col-xs-offset-2   ">
                            <form role="form" action="../../controladores/cad_usuario.php" method="post" >
                        <?php if(isset($_GET['erro'])) :?>

                        <div class="alert alert-danger alert-dismissible erroentra" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <strong> Atenção!</strong>  <?= @htmlspecialchars($_GET['erro']); ?>
                        </div>

                        <?php endif ?>
                        
                            <div class="form-group">
                                 <label >Usuário</label>
                                    <input type="text" name="usuario" class="form-control" placeholder=" Ex.: Usuário" value="<?= @$_SESSION['campos_cadastro']['usuario'] ?>" >

                                </div>
                                <div class="form-group">
                                     <label>Nome completo</label>
                                    <input type="text" class="form-control" placeholder="Ex.: Usuario Normal" name="nome" value="<?= @$_SESSION['campos_cadastro']['nome'] ?>"  >
                                </div>
                                <div class="form-group">
                                     <label>Email</label>
                                    <input type="email" class="form-control" placeholder="Ex: usuario@gmail.com" name="email" value="<?= @$_SESSION['campos_cadastro']['email'] ?>">
                                </div>
                                <div class="form-group">
                                     <label>Senha</label>
                                    <input type="password" class="form-control" placeholder="Senha" name="senha" value="<?= @$_SESSION['campos_cadastro']['senha'] ?>" >
                                    <label class="label label1">Min. 8 caracteres, letras e números.**</label>
                                </div> 
                                <div class="form-group">
                                     <label>Confirmar senha</label>
                                    <input type="password" class="form-control" placeholder="Confirmar senha" name="confirmar_senha" value="<?= @$_SESSION['campos_cadastro']['confirmar_senha'] ?>"?>
                                </div>
                                <button class="btn btn-primary btn-lg">Cadastrar</button>  
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--FIM CADASTRAR --> 
    <?php
        require("footer.php") 
    ?>

    <script src="../js/jquery-1.11.1.min.js"></script>
    <script src="../bootstrap-3.3.7/dist/js/bootstrap.min.js"></script>
    <script src="../js/slick.min.js"></script>
    <script src="../js/placeholdem.min.js"></script>

    <script src="../js/scripts.js"></script>
    <script>
        $(document).ready(function() {
            appMaster.preLoader();
        });
    </script>
</body>
</html>
