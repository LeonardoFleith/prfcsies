<?php session_start(); ?>
<html lang="en" class="no-js">

<head>
<meta charset="UTF-8">
<title>SIES - Sistema de Irrigação Eco-Sustentável</title>
<!-- LINKS -->
<link rel="stylesheet" href="../bootstrap-3.3.7/dist/css/bootstrap.min.css">
<link rel="stylesheet" href="../css/animate.css">
<link rel="stylesheet" href="../css/font-awesome.min.css">
<link rel="stylesheet" href="../fonts/font.css">
<link rel="stylesheet" href="../css/styles.css">
<!-- LINKS -->
</head>

<body>
<?php
include_once"menu.php";
?>
<!--ENTRAR-->
<section id="Entrar" class="doublediagonal">
<div class="container">
<div class="section-heading ">
    <h1>Entrar</h1>
<div class="divider"></div>
</div>
<div class="row">
    <div class="col-md-6 ">
        <div class="row">
            <div class="col-md-8 col-md-offset-8 col-sm-7 col-sm-offset-3 col-xs-8 col-xs-offset-2   ">
                <?php if(isset($_GET['erro'])) :?>

                    <div class="alert alert-danger alert-dismissible erroentra" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <strong> Atenção!</strong>  <?= @htmlspecialchars($_GET['erro']); ?>
                    </div>

                <?php endif ?>

                <form role="form" action="../../controladores/entrar.php" method="post">

                 

                    <div class="form-group">
                        <label>Usuário</label>
                        <input type="text" class="form-control" placeholder=" Ex.: Usuario123" name="usuario" value="<?= @$_SESSION['campos_entrar']['usuario'] ?>" >

                    </div>
                    <div class="form-group">
                        <label>Senha</label>
                        <input type="password" class="form-control" placeholder="Senha" name="senha" value="<?= @$_SESSION['campos_entrar']['senha'] ?>">
                        
                    </div>
                    
                    <div class="media">
                        <div class="media-body">
                            <h4 class="media-heading">
                                <a href="esqueceu_senha.php">Esqueceu a senha?</a>
                                 <button type="submit" class="btn btn-primary btn-lg">Entrar</button>
                            </h4>
                        </div>
                    </div>
                        <div class="media">
                            <div class="media-body">
                                <h4 class="media-heading">Ainda não é cadastrado?</h4>
                            </div>
                            <div class="media-body">
                                <h4 class="media-heading cadastro">
                                    <a href="cadastro_usuario.php" >  Cadastre-se</a>
                                </h4>
                            </div>
                        </div>
                    
                </form>
            </div>
        </div>
    </div>
</div>
</div>
</section>
<!--FIM ENTRAR --> 
<?php
include_once"footer.php";
?>

<script src="../js/jquery-1.11.1.min.js"></script>
<script src="../bootstrap-3.3.7/dist/js/bootstrap.min.js"></script>
<script src="../js/slick.min.js"></script>
<script src="../js/placeholdem.min.js"></script>

<script src="../js/scripts.js"></script>
<script>
$(document).ready(function() {
appMaster.preLoader();
});
</script>
</body>
</html>
