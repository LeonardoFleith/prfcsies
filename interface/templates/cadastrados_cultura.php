<div class="col-md-11 padding-col-2">
  <div class="section-heading  dois">
    <h1>Cadastros Efetuados</h1>
  </div>
  <div class="nav cadastrados">


    <ul id="" class="nav nav-tabs nav-justified">
      <li><a href="?pgs=cadastrados_solo.php&pg=1">Solo</a></li>
      <li class="active"><a href="?pgs=cadastrados_cultura.php&pg=1">Cultura</a></li>
      <li><a href="?pgs=cadastrados_cultivo.php&pg=1">Cultivo</a></li>
    </ul>

    <div class="tab-content">
      <div role="tabpanel"  class="tab-pane fade in active" id="cultura">
<section class="lista lista1">
    <?php
    include_once '../../classes/Cultura.php';
    $id_usuario = $_SESSION['logar']['id_usuario'];

    $pg_atual = $_GET['pg'];
    $quantidade = 3;
    $cadastrado = new Cultura();
    $cadastrados= $cadastrado ->paginacao_cultura($pg_atual, $quantidade, $id_usuario);

    $n_pagina = new Cultura();
    $limite = $n_pagina->quantidade_paginas_cultura($quantidade, $id_usuario);
        
        $anterior = $pg_atual -1;
        $proxima = $pg_atual + 1;
        if ($pg_atual=1) {
          $anterior = $pg_atual;
        }

if ($_GET['pg']>$limite) {
          ?>
           <section class="text-holder">
                    <section class="feed-title">
                        <h4>Não há mais nenhuma cultura cadastrada </h4>
                    </section>
                </section> 
          <p></p>
          <a href='?pgs=cadastrados_cultura.php&pg=<?=$anterior;?>' class="">Voltar para a inicial</a>
          <?php
          exit();
}

if ( !empty($cadastrados)) {
    foreach ($cadastrados as $cadastrado) {?>
    <section class="solicitado col-md-6">
        <section class="text-holder">
            <section class="feed-title">
                <h4><?= $cadastrado['tipo_planta'];?> 
                </h4>
                <section class="feed-description">
                    <p>Nome cientifico: <?= $cadastrado['nome_cientifico'];?>
                   
                    <button class=" btn btn-primary btn-lg botao exclui">
                            <a href="../../controladores/update_cultura_excluir.php?id_cultura=<?= $cadastrado['id_cultura'];?>"> Excluir 
                            </a> 
                        </button>
                        <button class=" btn btn-primary btn-lg botao edita">
                            <a href="?pgs=alterar_cultura.php&id_cultura=<?=$cadastrado['id_cultura'];?>"> Editar 
                            </a> 
                        </button> </p>
                </section>
            </section>
        </section>
    </section> 
    <?php }}  
    ?>
<nav aria-label="...">
  <ul class="pager">
    <li class="previous"><a href="?pgs=cadastrados_cultura.php&pg=<?=$anterior?>"><span aria-hidden="true">&larr;</span> Anterior</a></li>
    <li class="next"><a href="?pgs=cadastrados_cultura.php&pg=<?=$proxima?>">Próximo <span aria-hidden="true">&rarr;</span></a></li>
  </ul>
</nav>
</section>
</div>
  </div>

</div>
