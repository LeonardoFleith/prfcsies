<?php
    include_once "../../classes/Login.php";
    $logado = new Login();
    $logado -> ta_Logado();

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">

    <title>Bem vindo ao SIES</title>
    <link href="../bootstrap-3.3.7/dist/css/bootstrap.min.css" rel="stylesheet">
 
    <link rel="stylesheet" href="../css/animate.css">
    <link rel="stylesheet" href="../css/font-awesome.min.css">
    <link rel="stylesheet" href="../fonts/font.css">
    <link rel="stylesheet" href="../css/simple-sidebar.css">
    <link rel="stylesheet" href="../css/styles.css">
</head>

<body>

    <div id="wrapper">
  <?php
            $cod_tipo_user =$_SESSION['logar']['cod_tipo_user'];

            include_once "../../controladores/menu_inclui.php";

                $menu = [

                    'menu1'=>'dashboard.php?pgs=cadastro_cultura.php',
                    'menu2'=>'dashboard.php?pgs=cadastro_solo.php',
                    'menu3'=>'dashboard.php?pgs=cadastro_cultivo.php',
                    'menu4'=>'dashboard.php?pgs=cadastro_cultura_Admin.php',
                    'menu5'=>'dashboard.php?pgs=cadastro_solo_Admin.php',
                    'menu6'=>'dashboard.php?pgs=cadastrados_solo.php&pg=1',
                    'menu7'=>'dashboard.php?pgs=../graficos/relatorio.php',
                    'menu8'=>'dashboard.php?pgs=escolhe_cultivo_higrometro.php',
                    'menu9'=>'dashboard.php?pgs=escolhe_cultivo_ambiente.php'];

                    if ($cod_tipo_user==1) {
                        $template1 = getTemplate('menu_admin.php');
                        $templateFinal = parseTemplate( $template1, $menu );
                        echo $templateFinal;}
                    else {
                        $template1 = getTemplate('menu_usuario_normal.php');
                        $templateFinal = parseTemplate( $template1, $menu );
                        echo $templateFinal;
                    }
        ?>
        <!-- Sidebar -->
        
        
        <section id="painell" class="painel">
        <nav class="navbar navbar-default navbar-fixed navs">
            <div class="container-fluid">
                <div class="collapse navbar-collapse barranav bs-example-js-navbar-collapse ">
                <div id="botao-menu"> <span class="fa fa-bars fa-lg"></span> Menu </div>
             
                    <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Olá, <?php
                                $nome = $_SESSION['logar']['nome'];
                                echo $nome;?> <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                               
                                <li><a href="dashboard.php?pgs=alterar_senhas.php">Trocar Senha</a></li>
                                <li><a href="../../controladores/saida.php">Sair</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>

            </div>
        </nav>
     <?php
                include_once '../../controladores/pasta_inclui.php';
                
                ins_dados(filter_input(INPUT_GET, 'pgs'));

            ?> 
     
        </section>
    </div>

    <script src="../js/jquery.js"></script>
    <script src="../bootstrap-3.3.7/dist/js/bootstrap.min.js"></script>
    <script src="../js/jquery-1.11.1.min.js"></script>
    <script src="../js/slick.min.js"></script>
    
    <script src="../js/jquery.themepunch.revolution.min.js"></script>
    <script src="../js/waypoints.min.js"></script>
    <script src="../js/slick.min.js"></script>

    <script src="../bootstrap-3.3.7/js/tab.js"></script>
    <script src="../js/scripts.js"></script>

    <script type="text/javascript">
    $('#tabsCadastrados a').click(function (e) {
      e.preventDefault()
      $(this).tab('show')
    })

    $('#myTabs a[href="#profile"]').tab('show'); // Select tab by name
    $('#myTabs a:first').tab('show'); // Select first tab
    $('#myTabs a:last').tab('show'); // Select last tab
  </script>

</body>

</html>
