<?php 
include_once '../../classes/Cultura.php'; 

if( $_SESSION['logar']['cod_tipo_user'] == 1){    
    ?>
    <section class="lista">
        <?php
            $cultura = new Cultura();
            $culturas= $cultura -> dadosCultura ();
        ?>
        <div class="section-heading  dois">
            <h1>Solicitação Cadastro de Cultura</h1>
            <div class="divider"></div>
        </div>
        <?php
            if ( !empty($culturas)) {

                foreach ($culturas as $cultura) {?>

                    <section class="solicitado col-md-6">
                        <section class="text-holder">
                            <section class="feed-title">
                                <h4><?= $cultura['tipo_planta'];?> 
                                </h4>
                            </section>
                            <section class="feed-description">
                                <p> Nome: <?= $cultura['nome_cientifico'];?> 
                                    <button class="btn btn-primary btn-lg botao exclui">
                                        <a href="../../controladores/update_cultura_excluir_adm.php?id_cultura=<?= $cultura['id_cultura'];?>"> Excluir 
                                        </a> 
                                    </button>
                                    <button class="btn btn-primary btn-lg botao ativa">
                                        <a href="../../controladores/update_cultura_ativar.php?id_cultura=<?= $cultura['id_cultura'];?>"> Ativar 
                                        </a> 
                                    </button>
                                </p>
                            </section>
                        </section>
                    </section> 
                <?php }  
            }  
            else{ ?>

                <section class="text-holder">
                    <section class="feed-title">
                        <h4>Nenhuma cultura solicitada </h4>
                    </section>
                </section> 
            <?php
            } 
            ?>
    </section>
    <?php
}else{
    echo("<script type='text/javascript'> alert( 'Você não pode acessar está pagina :(' );
        location.href='../templates/dashboard.php?pos=1&pgs=cadastro_cultura.php';</script>");
}
