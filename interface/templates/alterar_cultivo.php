<section class="doublediagonal">
    <div class="container">
        <div class="col-md-9  col-sm-9 padding-col">
            <div class="section-heading  dois">
                <h1>Editar Cultivo</h1>

                <div class="divider"></div>
            </div>
            <form method="post" action="../../controladores/update_cultivo_editar.php" role="form col-sm-2">

            <?php
            include_once "../../classes/Cultivo.php";
            
            $id_cultivo = @htmlspecialchars($_GET['id_cultivo']);

            $cultivo = new Cultivo();
            $exibicao = $cultivo -> apresentaCultivo($id_cultivo); 
            ?>

                <div class="form-group">
                    <input type="text" class="form-control" name="novo_nome_cultivo" value="<?= $exibicao['nome_cultivo'];?>" > 

                </div>
                <div class="form-group">
                    <select name="novo_id_cultura" class="form-control">
                        <option class=" form-control" value="<?=$exibicao['cod_cultura'];?>"><?= $exibicao['tipo_planta'];?></option>  
                        <?php
                        $id_usuario = $_SESSION['logar']['id_usuario'];
                        include_once "../../classes/Cultura.php";
                        $tipo_planta = new Cultura();
                        $tipo_plantas = $tipo_planta -> retornaCultura($id_usuario);
                        foreach ($tipo_plantas as $tipo_planta) {?>  
                        <option class=" form-control" value="<?= $tipo_planta['id_cultura'];?>"><?=$tipo_planta['tipo_planta']; ?>
                        </option>
                        <?php } 
                        ?>
                    </select>
                </div>
                <div class="form-group">
                    <select name="novo_id_solo" class="form-control">
                        <option class=" form-control" value="<?=$exibicao['cod_solo'];?>"><?= $exibicao['tipo_solo'];?></option>
                        <?php
                        include_once "../../classes/Solo.php";
                        $tipo_solo = new Solo();
                        $tipo_solos = $tipo_solo -> retornaSolo ($id_usuario);
                        foreach ($tipo_solos as $tipo_solo) {?>
                        <option class=" form-control" value="<?=$tipo_solo['id_solo'];?>"><?=$tipo_solo['tipo_solo'];?></option>
                        <?php } 
                        ?>
                    </select>
                </div>
                 <input type="hidden" name="id_cultivo" value="<?=$id_cultivo?>" class="form-control">
                <button class="btn btn-primary btn-lg">Enviar</button>        
            </form>   
        </div>
    <div>
</section>
