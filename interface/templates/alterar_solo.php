<section class="doublediagonal">
    <div class="container logado">
        <div class=" col-md-9  col-sm-9 padding-col">
            <div class="section-heading  dois">
                <h1>Editar Solo</h1>
            <div class="divider"></div>
            </div>
            <form  method="post" action="../../controladores/update_solo_editar.php" role="form col-sm-2">

             <?php if(isset($_GET['erro'])) :?>

                        <div class="alert alert-danger alert-dismissible erroentra" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <strong> Atenção!</strong>  <?=@htmlspecialchars($_GET['erro']); ?>
                        </div>

                        <?php endif ?>
                        <?php if(isset($_GET['mensagem'])) :?>

                    <div class="alert alert-success alert-dismissible erroentra" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <strong> Parabéns!</strong>  <?php echo @htmlspecialchars($_GET['mensagem']);?>
                </div>
                <?php endif ?>

            <?php include_once "../../classes/Solo.php";

            $id_solo = $_GET['id_solo'];
            $solo = new Solo();
            $exibicao = $solo -> apresentaSolo($id_solo);
            
            ?>
                <div class="form-group">
                    <label>Tipo de Solo</label>
                    <input type="text" class="form-control"  name="novo_tipo_solo" value="<?=$exibicao['tipo_solo'];?>"> 
                </div>
                <div class="form-group">
                    <label>Capacidade de Campo</label>
                    <input type="number" class="form-control" name="novo_cap_campo" value="<?=$exibicao['cap_de_campo'];?>">
                </div>                    
                <input type="hidden" name="id_solo" value="<?=$id_solo?>" class="form-control">
                 <button class="btn btn-primary btn-lg">Editar</button>          
            </form>   
        </div>
    <div>  
</section>