-- Gera��o de Modelo f�sico
-- Sql ANSI 2003 - brModelo.



CREATE TABLE cultura (
id_cultura int PRIMARY KEY AUTO_INCREMENT NOT NULL,
cod_usuario int NOT NULL,
tipo_planta varchar(60)NOT NULL,
status_cultura varchar(60)NOT NULL,
nome_cientifico varchar(200) NOT NULL
);

CREATE TABLE solo (
id_solo int PRIMARY KEY AUTO_INCREMENT NOT NULL,
cod_usuario int NOT NULL,
cap_de_campo int NOT NULL,
status_solo varchar(20) NOT NULL,
tipo_solo varchar(60) NOT NULL
);

CREATE TABLE usuario (
id_usuario int PRIMARY KEY AUTO_INCREMENT NOT NULL,
nome varchar(60)NOT NULL,
usuario varchar(20)NOT NULL,
email varchar(60)NOT NULL,
senha varchar(100)NOT NULL,
cod_tipo_user int NOT NULL
);

CREATE TABLE tipo_user (
tipo_user int PRIMARY KEY NOT NULL,
descricao varchar(20)NOT NULL
);

INSERT INTO `tipo_user` (`tipo_user`, `descricao`) VALUES
(1, 'admin'),
(2, 'comum');

CREATE TABLE cultivo (
id_cultivo int PRIMARY KEY AUTO_INCREMENT NOT NULL,
cod_solo int NOT NULL,
cod_cultura int NOT NULL,
nome_cultivo varchar(60) NOT NULL,
status_cultivo varchar(20) NOT NULL,
FOREIGN KEY(cod_solo) REFERENCES solo (id_solo),
FOREIGN KEY(cod_cultura) REFERENCES cultura (id_cultura)
);

CREATE TABLE leituras (
id_leitura int PRIMARY KEY AUTO_INCREMENT NOT NULL,
temperatura int NOT NULL,
umidade_solo1 int NOT NULL,
umidade_solo2 int NOT NULL,
umidade_solo3 int NOT NULL,
umidade_ar int NOT NULL,
data_leitura date NOT NULL,
cod_bomba int NOT NULL,
cod_arduino varchar(20)NOT NULL,
hora time NOT NULL,
cod_cultivo int NOT NULL,
status_bomba varchar(20) NOT NULL,
FOREIGN KEY(cod_cultivo) REFERENCES cultivo (id_cultivo)
);

ALTER TABLE cultura ADD FOREIGN KEY(cod_usuario) REFERENCES usuario (id_usuario);
ALTER TABLE solo ADD FOREIGN KEY(cod_usuario) REFERENCES usuario (id_usuario);
ALTER TABLE usuario ADD FOREIGN KEY(cod_tipo_user) REFERENCES tipo_user (tipo_user);
